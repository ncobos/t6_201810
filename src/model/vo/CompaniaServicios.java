package model.vo;

import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class CompaniaServicios implements Comparable<CompaniaServicios> {

	private String nomCompania;

	private Lista<Service> servicios;

	private int numero;

	public CompaniaServicios (String pNombre, Lista<Service> pServicios)
	{
		nomCompania = pNombre;
		servicios = pServicios;
		numero = servicios.size();
	}

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public LinkedList<Service> getServicios() {
		return servicios;
	}

	public void setServicios(Lista<Service> servicios) {
		this.servicios = servicios;
	}

	@Override
	public int compareTo(CompaniaServicios o) {
		// TODO Auto-generated method stub
		int rta = 0;
		if(numero > o.getNumberOfServices())
		{
			rta = 1;
		}
		if(numero < o.getNumberOfServices())
		{
			rta = -1;
		}

		return rta;
	}

	public int getNumberOfServices()
	{
		return servicios.size();
	}

	public Taxi taxiRentable()
	{
		Taxi rta = null;
		double rentabilidad = 0;
		if(servicios.size()==1)
		{
			Service servicio = servicios.getByPos(0);
			rta = servicio.getTaxi();		
		}

		else{
			
			for (int i = 0; i < servicios.size(); i++)
			{
				Service servicioActual = servicios.getByPos(i);
				Taxi taxiActual = servicioActual.getTaxi();

				double plata = 0;
				double distancia = 0;

				for (int j = i+1; j < servicios.size(); j++)
				{
					Service buscar = servicios.getByPos(j);
					Taxi taxiBuscar = buscar.getTaxi();

					if(taxiActual.compareTo(taxiBuscar)== 0)
					{
						plata += buscar.getTripTotalCost();
						distancia += buscar.getTripMiles();
					}
				}

				double rent = plata/distancia;

				if(rent > rentabilidad)
				{
					rentabilidad = rent;
					rta = taxiActual;
				}
			}
		}
			return rta;
		
	}

}
