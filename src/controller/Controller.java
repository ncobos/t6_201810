package controller;

import api.ITaxiTripsManager;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.logic.TaxiTripsManager;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager = new TaxiTripsManager();

	//1C
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	//1.4
	public static Lista<Service> serviciosPorArea(int area)
	{
		Lista<Service> ht = manager.serviciosPorArea(area);
		return ht;
	}

	//2.1
	public static Lista<Service> serviciosPorDis(double dis)
	{
		Lista<Service> ht = manager.serviciosPorDis(dis);
		return ht;
	}


}
