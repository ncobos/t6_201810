package model.data_structures;


public class MaxPQ <T extends Comparable<T>>
{
	private Comparable[] pq; // heap-ordered complete binary tree
	private int N = 0; // in pq[1..N] with pq[0] unused

	public MaxPQ(int maxN)
	{ pq =  new Comparable[maxN+1]; }

	public Comparable[] getPQ()
	{
		return pq;
	}

	public boolean isEmpty()
	{ return N == 0; }

	public int size()
	{ return N; }

	public void insert(T v)
	{
		pq[++N] = v;
		swim(N);
	}
	public Comparable delMax()
	{
		Comparable max = pq[1]; // Retrieve max key from top.
		exch(1, N--); // Exchange with last item.
		pq[N+1] = null; // Avoid loitering.
		sink(1); // Restore heap property.
		return max;
	}

	private void sink(int k)
	{
		while (2*k <= N)
		{
			int j = 2*k;
			if (j < N && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	private void swim(int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}

	private boolean less(int i, int j)
	{ return pq[i].compareTo(pq[j]) < 0; }

	private void exch(int i, int j)
	{ Comparable t = pq[i]; pq[i] = pq[j]; pq[j] = t; }

	/**
     * Rearranges the array in ascending order, using the natural order.
     * @param pq the array to be sorted
     */
    public static void sort(Comparable[] pq) {
        int n = pq.length;
        for (int k = n/2; k >= 1; k--)
            sink(pq, k, n);
        while (n > 1) {
            exch(pq, 1, n--);
            sink(pq, 1, n);
        }
    }

   /***************************************************************************
    * Helper functions to restore the heap invariant.
    ***************************************************************************/

    private static void sink(Comparable[] pq, int k, int n) {
        while (2*k <= n) {
            int j = 2*k;
            if (j < n && less(pq, j, j+1)) j++;
            System.out.println("k sink: " + k + "n sink: " + n);
            if (!less(pq, k, j)) break;
            exch(pq, k, j);
            k = j;
        }
    }

   /***************************************************************************
    * Helper functions for comparisons and swaps.
    * Indices are "off-by-one" to support 1-based indexing.
    ***************************************************************************/
    private static boolean less(Comparable[] pq, int i, int j) {
    	System.out.println("i: " + i + " j: " + j);
        return pq[i-1].compareTo(pq[j-1]) < 0;
    }

    private static void exch(Object[] pq, int i, int j) {
        Object swap = pq[i-1];
        pq[i-1] = pq[j-1];
        pq[j-1] = swap;
    }

    // print array to standard output
//    private static void show(Comparable[] a) {
//        for (int i = 0; i < a.length; i++) {
//            StdOut.println(a[i]);
//        }
//    }

    /**
     * Reads in a sequence of strings from standard input; heapsorts them; 
     * and prints them to standard output in ascending order. 
     *
     * @param args the command-line arguments
     */
//    public static void main(String[] args) {
//        String[] a = StdIn.readAllStrings();
//        Heap.sort(a);
//        show(a);
//    }



//	private static int total;
//
//	private static void swap(Comparable[] arr, int a, int b)
//	{
//		Comparable tmp = arr[a];
//		arr[a] = arr[b];
//		arr[b] = tmp;
//	}
//
//	private static void heapify(Comparable[] arr, int i)
//	{
//		if(i!=0){
//		int lft = i * 2;
//		int rgt = lft + 1;
//		int grt = i;
//System.out.println("grt: " + grt);
//		if (lft <= total && arr[lft].compareTo(arr[grt]) > 0) grt = lft;
//		if (rgt <= total && arr[rgt].compareTo(arr[grt]) > 0) grt = rgt;
//		if (grt != i) {
//			swap(arr, i, grt);
//			heapify(arr, grt);
//		}}
//	}
//
//	public static void sort(Comparable[] arr)
//	{
//		total = arr.length - 1;
//
//		for (int i = total / 2; i >= 0; i--)
//			heapify(arr, i);
//
//		for (int i = total; i > 0; i--) {
//			swap(arr, 0, i);
//			total--;
//			heapify(arr, 0);
//		}
//	}
//
//	public static void main(final String[] args)
//	{
//		Integer[] arr = new Integer[] { 3, 2, 1, 5, 4 };
//
//		System.out.println(java.util.Arrays.toString(arr));
//		sort(arr);
//		System.out.println(java.util.Arrays.toString(arr));
//	}

//{
//	private T[] pq; // heap-ordered complete binary tree
//	private int N = 0; // in pq[1..N] with pq[0] unused
//	public MaxPQ(int maxN)
//	{ pq = (T[]) new Comparable[maxN+1]; }
//	public boolean isEmpty()
//	{ return N == 0; }
//	public int size()
//	{ return N; }
//	public void insert(T v)
//	{
//		pq[++N] = v;
//		swim(N);
//	}
//	public T delMax()
//	{
//		T max = pq[1]; // Retrieve max key from top.
//		exch(1, N--); // Exchange with last item.
//		pq[N+1] = null; // Avoid loitering.
//		sink(1); // Restore heap property.
//		return max;
//	}
//
//	public T[] getPQ()
//	{
//		return pq;
//	}
//
//	private void swim(int k)
//	{
//		while (k > 1 && less(k/2, k))
//		{
//			exch(k/2, k);
//			k = k/2;
//		}
//	}
//
//	private void sink(int k)
//	{
//		while (2*k <= N)
//		{
//			int j = 2*k;
//			if (j < N && less(j, j+1)) j++;
//			if (!less(k, j)) break;
//			exch(k, j);
//			k = j;
//		}
//	}
//	
////	private static void sinkSort(Comparable[] pq, int k, int n) {
////	while (2*k <= n) {
////		int j = 2*k;
////		if (j < n && less(pq, j, j+1)) j++;
////		if (!less(pq, k, j)) break;
////		exchSort(pq, k, j);
////		k = j;
////	}
//
//	private static boolean less(Comparable v, Comparable w)
//	{ return v.compareTo(w) < 0; }
//	private void exch(int i, int j)
//	{ T t = pq[i]; pq[i] = pq[j]; pq[j] = t; }
////
//
//
//public static void sort(Comparable[] a)
//{
//	int N = a.length;
//	for (int k = N/2; k >= 1; k--)
//		sink(a, k, N);
//	while (N > 1)
//	{
//		exch(a, 1, N--);
//		sink(a, 1, N);
//	}
//	
//	private static int total;
//	
//	private static void swap(Comparable[] arr, int a, int b)
//    {
//        Comparable tmp = arr[a];
//        arr[a] = arr[b];
//        arr[b] = tmp;
//    }
//
//    private static void heapify(Comparable[] arr, int i)
//    {
//        int lft = i * 2;
//        int rgt = lft + 1;
//        int grt = i;
//
//        if (lft <= total && arr[lft].compareTo(arr[grt]) > 0) grt = lft;
//        if (rgt <= total && arr[rgt].compareTo(arr[grt]) > 0) grt = rgt;
//        if (grt != i) {
//            swap(arr, i, grt);
//            heapify(arr, grt);
//        }
//    }
//
//    public static void sort(Comparable[] arr)
//    {
//        total = arr.length - 1;
//
//        for (int i = total / 2; i >= 0; i--)
//            heapify(arr, i);
//
//        for (int i = total; i > 0; i--) {
//            swap(arr, 0, i);
//            total--;
//            heapify(arr, 0);
//        }
//    }
	
//	/**
//	  *  Sorts a given array of items.
//	  */
//	   public void heapSort(T[] array)
//	   {
//	      int size = array.length;
//	      heap = (AnyType[]) new Comparable[size+1];
//	      System.arraycopy(array, 0, heap, 1, size);
//	      buildHeap();
//
//	      for (int i = size; i > 0; i--)
//	      {
//	         AnyType tmp = heap[i]; //move top item to the end of the heap array
//	         heap[i] = heap[1];
//	         heap[1] = tmp;
//	         size--;
//	         percolatingDown(1);
//	      }
//	      for(int k = 0; k < heap.length-1; k++)
//	         array[k] = heap[heap.length - 1 - k];
//	   }

}