package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Cola;
import model.vo.Taxi;

public class ColaTest <T extends Comparable <T>> 

{
	private Cola cola;
	
	Taxi taxi = new Taxi("id", "compania");
	
	@Before
	public void setup1()
	{
		cola = new Cola();
	}
	
	@Test
	public void testEnqueue()
	{
		setup1();
		
		cola.enqueue(taxi);
		
		assertEquals(1, cola.size());
	}
	
	@Test 
	public void testDequeue()
	{
		setup1();
		
		cola.enqueue(taxi);
		
		cola.dequeue();
		
		assertEquals(0, cola.size());
	}
	
	@Test
	public void testEmpty()
	{
		setup1();
		
		assertTrue("Debio haber estado vacia.", cola.isEmpty());
	}

}
